import 'style/global.scss';
import 'style/variables.scss';

import { render } from 'react-dom';

import FileProcessorScreen from 'Component/FileProcessorScreen';

// -----------------------------------------------------------------------------

export default function renderApp(): void {
	render(<App />, document.body);

	// remove loader components
	const components = document.querySelectorAll('[data-preloader="true"]');
	[].forEach.call(components, (component) => component.parentNode.removeChild(component));
}

// -----------------------------------------------------------------------------

function App() {
	return <FileProcessorScreen />;
}

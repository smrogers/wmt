import c from './style.scss';

import { memo } from 'react';
import { useTable } from 'react-table';

import SchemaDataTableCell from 'Component/SchemaDataTableCell';

// -----------------------------------------------------------------------------

const defaultColumn = {
	Cell: SchemaDataTableCell,
};

// -----------------------------------------------------------------------------

const SchemaDataTable = memo(function SchemaDataTable({
	schema,
	tableConfig,
	data,
	onChange,
}: SchemaDataTableProps): JSX.Element {
	const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable({
		columns: tableConfig,
		defaultColumn,
		data,
		onUpdate: onChange,
		schema,
	});

	//
	return (
		<table {...getTableProps()} className={c.table}>
			<thead className={c.header}>{headerGroups.map(renderHeader)}</thead>
			<tbody {...getTableBodyProps()}>{rows.map(renderRow(prepareRow))}</tbody>
		</table>
	);
});

export default SchemaDataTable;

// -----------------------------------------------------------------------------

function renderHeader(headerGroup) {
	return (
		<tr {...headerGroup.getHeaderGroupProps()}>{headerGroup.headers.map(renderHeaderCell)}</tr>
	);
}

function renderHeaderCell(column) {
	return <th {...column.getHeaderProps()}>{column.render('Header')}</th>;
}

function renderRow(prepareRow) {
	return (row) => {
		prepareRow(row);

		//
		return (
			<tr {...row.getRowProps()} className={c.row}>
				{row.cells.map(renderCell)}
			</tr>
		);
	};
}

function renderCell(cell) {
	const id = cell?.column?.id;

	//
	return (
		<td {...cell.getCellProps()} className={c[id]}>
			{cell.render('Cell')}
		</td>
	);
}

// -----------------------------------------------------------------------------

interface SchemaDataTableProps {
	schema: GenericObject;
	tableConfig: any[];
	data: any[];
	onChange: Function;
}

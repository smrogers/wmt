import c from './style.scss';

import { memo, useState } from 'react';

import CSVFileUploader from 'Component/CSVFileUploader';
import Render from 'Component/Render';
import ResetFile from 'Component/ResetFile';
import SchemaDataTable from 'Component/SchemaDataTable';

import schema from 'data/schema.json';
import tableConfig from 'data/table_config.json';

// -----------------------------------------------------------------------------

const FileProcessorScreen = memo(
	function FileProcessorScreen({}: FileProcessorScreenProps): JSX.Element {
		const [data, setData] = useState(undefined as any[]);
		const clearData = () => setData(undefined);

		const onUpdateData = (rowIndex, columnId, value) => {
			setData((oldData) =>
				oldData.map((row, index) => {
					return index === rowIndex
						? {
								...oldData[rowIndex],
								[columnId]: value,
						  }
						: row;
				}),
			);
		};

		//
		return (
			<div className={c.container}>
				<Render
					if={data}
					children={() => (
						<>
							<ResetFile onClick={clearData} />
							<SchemaDataTable
								schema={schema}
								tableConfig={tableConfig}
								data={data}
								onChange={onUpdateData}
							/>
						</>
					)}
					else={() => <CSVFileUploader onChange={setData} />}
				/>
			</div>
		);
	},
);

export default FileProcessorScreen;

// -----------------------------------------------------------------------------

interface FileProcessorScreenProps {}

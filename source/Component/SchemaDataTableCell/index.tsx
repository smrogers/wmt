import c from './style.scss';

import { memo, useEffect, useRef, useState, RefObject } from 'react';
import { ReturnIf } from 'babel-plugin-transform-functional-return';

import { classIf, cn } from 'lib/css';
import dateValidator from 'lib/validator/date';
import noneValidator from 'lib/validator/none';
import integerValidator from 'lib/validator/integer';
import clientReqIdValidator from 'lib/validator/client_req_id';

import Render from 'Component/Render';

// -----------------------------------------------------------------------------

const Validator = {
	client_req_id: clientReqIdValidator,
	integer: integerValidator,
	date: dateValidator,
};

// -----------------------------------------------------------------------------

const SchemaDataTableCell = memo(function SchemaDataTable({
	value,
	row: { index },
	column: { id },
	onUpdate,
	schema,
}: SchemaDataTableCellProps): JSX.Element {
	const [type, required] = schema?.[id] || [];
	const validateValue = Validator[type] || noneValidator;

	//
	const [editing, setEditing] = useState(false);

	//
	const isValid = hasValueIfRequired(value, required) && validateValue(value);
	const beginEditing = isValid ? undefined : () => setEditing(true);
	const changeValue = (e) => {
		const newValue = e.target.value;

		onUpdate(index, id, newValue);
		setEditing(false);
	};
	const handleKeyUp = (e) => {
		switch (e.key) {
			case 'Enter':
				e.preventDefault();
				changeValue(e);
				return;
		}
	};

	//
	const inputRef: RefObject<HTMLInputElement> = useRef();
	useEffect(() => {
		ReturnIf(!editing);
		inputRef.current.focus();
	}, [editing]);

	//
	return (
		<Render
			if={editing}
			children={() => (
				<div className={c.editCell}>
					<input
						type="text"
						value={value}
						onKeyUp={handleKeyUp}
						onBlur={changeValue}
						ref={inputRef}
					/>
				</div>
			)}
			else={() => (
				<div
					className={cn(c.cell, classIf(!isValid, c.failedValidation))}
					onClick={beginEditing}
				>
					{value || <>&nbsp;</>}
				</div>
			)}
		/>
	);
});

export default SchemaDataTableCell;

// -----------------------------------------------------------------------------

function hasValueIfRequired(value: any, required: boolean): boolean {
	return required ? value !== undefined && value !== null && String(value) !== '' : true;
}

// -----------------------------------------------------------------------------

interface SchemaDataTableCellProps {
	value: any;
	row: GenericObject;
	column: GenericObject;
	onUpdate: Function;
	schema: GenericObject;
}

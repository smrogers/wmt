import c from './style.scss';

import { memo, MouseEventHandler } from 'react';

// -----------------------------------------------------------------------------

const ResetFile = memo(function ResetFile({ onClick }: ResetFileProps): JSX.Element {
	return (
		<div className={c.container}>
			<button onClick={onClick as MouseEventHandler<HTMLButtonElement>}>
				Process new file
			</button>
		</div>
	);
});

export default ResetFile;

// -----------------------------------------------------------------------------

interface ResetFileProps {
	onClick: Function;
}

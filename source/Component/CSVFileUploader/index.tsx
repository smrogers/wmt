import c from './style.scss';

import { memo, useRef, RefObject } from 'react';
import { ReturnIf } from 'babel-plugin-transform-functional-return';
import csv from 'csvtojson';

// -----------------------------------------------------------------------------

const CSVFileUploader = memo(function CSVFileUploader({
	onChange,
}: CSVFileUploaderProps): JSX.Element {
	const inputRef: RefObject<HTMLInputElement> = useRef();
	const processSelectedFile = async () => {
		const file: File = getInputfile(inputRef);
		ReturnIf(!file);

		const fileString: string = await readInputFile(file);
		ReturnIf(!fileString);

		//
		onChange(await convertStringToCSV(fileString));
	};

	//
	return (
		<div className={c.container}>
			<strong>Select a file to upload</strong>
			<input type="file" accept=".csv" ref={inputRef} />
			<button onClick={processSelectedFile}>Process File</button>
		</div>
	);
});

export default CSVFileUploader;

// -----------------------------------------------------------------------------

function getInputfile(ref: RefObject<HTMLInputElement>): File {
	return ref?.current?.files?.[0];
}

async function readInputFile(file: File): Promise<string> {
	return await new Promise((resolve, reject) => {
		const fr: FileReader = new FileReader();
		fr.onerror = () => reject();
		fr.onload = () => resolve(fr.result as string);
		fr.readAsText(file, 'utf8');
	});
}

async function convertStringToCSV(csvString: string): Promise<GenericObject> {
	return await csv().fromString(csvString);
}

// -----------------------------------------------------------------------------

interface CSVFileUploaderProps {
	onChange: Function;
}

const EmptySpace: '' = '';
const SingleSpace: ' ' =  ' ';

// -----------------------------------------------------------------------------

export function cn(...classes: string[]): string {
	return classes.reduce(toClassnames, []).join(SingleSpace);
}

export function classIf(
	truthy: any,
	className: string,
	otherClassName: string = undefined,
): string {
	return truthy ? className : otherClassName;
}

// -----------------------------------------------------------------------------

function toClassnames(obj: string[], className: string) {
	const classNameTreated: string = (className || EmptySpace).trim();
	if (classNameTreated) {
		obj.push(classNameTreated);
	}

	//
	return obj;
}

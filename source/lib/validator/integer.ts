import { ReturnIf } from 'babel-plugin-transform-functional-return';

// -----------------------------------------------------------------------------

const NonNumericCharacters: RegExp = /[^\d]/;

// -----------------------------------------------------------------------------

export default function integerValidator(value: string): boolean {
	ReturnIf(NonNumericCharacters.test(value), false);

	//
	const parsedValue: number = parseInt(value, 10);

	//
	return !isNaN(parsedValue);
}

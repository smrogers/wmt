export default function dateValidator(value: string): boolean {
	try {
		const date = new Date(value);

		//
		return date.toISOString() === value;
	} catch (error) {
		return false;
	}
}

const CellFormat: RegExp = /^[a-z0-9]{4}$/;

// -----------------------------------------------------------------------------

export default function clientReqIdValidator(value: string): boolean {
	return CellFormat.test(value);
}

export function throwIf(truthy: any, message: string = 'There was an error'): void {
	if (truthy) {
		throw new Error(message);
	}
}
